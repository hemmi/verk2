#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include <iostream>
#include <string>
#include <list>
#include "service.h"
#include "individual.h"
#include "sciID.h"
#include<limits>
#include "validate.h"

using namespace std;

class consoleUI
{
public:
    consoleUI();
    void start();
    void add();
    void view();
    void search();
    void connections();

    //functions pertaining to the Computer class
    void addCom();
    void searchCom();
    void listsr(std::list<Computer> sr);
    void Corder();
    void connectCom();
    void connectedCom();

    //functions pertaining to the individual class
    void addSci();
    void searchSci();
    void listsr(std::list<individual> sr);
    void Sorder();
    void connectSci();
    void connectedSci();
private:
    service serv;
    validate val;
};

#endif // CONSOLEUI_H
