#include "service.h"

service::service()
{
}
void service::remove()
{
}
list<Computer> service::search(string searchterm)//search through the Computers.
{
    std::list<Computer> sr = repo.find(searchterm);
    return sr;
}

string service::assessOrder(string choice)
{
    string order;

    if (choice == "name"){
       order = "Name, CType, Built, YBuilt";
    }
    else if(choice == "year"){
        order = "YBuilt, Name, Ctype, Built";
    }
    else if(choice == "type"){
        order = "Ctype, Name, Built, YBuilt";
    }
    else if(choice == "built"){
        order = "Built, Name, Ctype, YBuilt";
    }
    return order;
}

std::list<Computer> service::sortC(string order)
{
    std::list<Computer> sortedlist = repo.sortC(order);
    return sortedlist;
}
void service::addCom(const string& name, const string &type, const string &built, const string &ybuilt)//add a new name.
{
    Computer c(name,ybuilt,type,built);
    repo.addCom(c);
}
list<Computer> service::listCom() //prepare the contents of the list for being printed on the user's screen.
{
    std::list<Computer> complist = repo.getClist();
    return complist;
}

int service::getRowIDCom()
{
    int id = repo.getRowIDCom();
    return id;
}
std::list<sciID> service::getComIDlist()
{
    std::list<sciID> sID = repo.computersID();
    return sID;
}
void service::connectCom(int id, vector<int> idlist)
{
    repo.connectCom(id, idlist);
}
Computer service::getConnectedComputer(int id)
{
    Computer com = repo.getConnectedComputer(id);
    return com;
}
std::list<Computer> service::ConnectedComputers(int id)
{
    std::list<Computer> listOfConCom = repo.ConnectedComputers(id);
    return listOfConCom;
}

list<individual> service::searchSci(string searchterm)//search through the Scientists.
{
    std::list<individual> sr = repo.findSci(searchterm);
    return sr;
}

string service::assessOrderS(string choice)
{
    string order;

    if (choice == "name"){
       order = "Name";
    }
    else if(choice == "birth"){
        order = "YoB, Name";
    }
    else if(choice == "death"){
        order = "YoD, Name";
    }
    else if(choice == "sex"){
        order = "Sex, Name";
    }
    return order;
}

std::list<individual> service::sortS(string order)
{
    std::list<individual> sortedlist = repo.sortS(order);
    return sortedlist;
}
void service::addSci(const string& name, const string &yearb, const string &yeard, const string &sex)//add a new name.
{
    individual i(name, yearb, yeard, sex);
    repo.addSci(i);
}
list<individual> service::listSci() //prepare the contents of the list for being printed on the user's screen.
{
    std::list<individual> scilist = repo.getSlist();
    return scilist;
}

int service::getRowIDSci()
{
    int id = repo.getRowIDSci();
    return id;
}

std::list<sciID> service::getSciIDlist()
{
    std::list<sciID> sID = repo.scientistsID();
    return sID;
}

void service::connectSci(int id, vector<int> idlist)
{
    repo.connectSci(id, idlist);
}
individual service::getConnectedScientist(int id)
{
    individual ind = repo.getConnectedScientist(id);
    return ind;
}
std::list<individual> service::ConnectedScientists(int id)
{
    std::list<individual> listOfConCom = repo.ConnectedScientists(id);
    return listOfConCom;
}
