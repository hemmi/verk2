#-------------------------------------------------
#
# Project created by QtCreator 2014-12-02T21:12:30
#
#-------------------------------------------------

QT       += core

QT       += core sql

QT       -= gui

TARGET = finalproject
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

CONFIG += static

SOURCES += main.cpp \
    consoleui.cpp \
    individual.cpp \
    repository.cpp \
    service.cpp \
    computer.cpp \
    sciID.cpp \
    validate.cpp

HEADERS += \
    consoleui.h \
    individual.h \
    repository.h \
    service.h \
    computer.h \
    sciID.h \
    validate.h
