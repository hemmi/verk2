#include "computer.h"

Computer::Computer()
{
}
Computer::Computer(const string& n, const string& yb, const string& t, const string& b)
{
    name = n;
    ybuilt = yb;
    type = t;
    built = b;
}
ostream& operator<<(ostream& out, Computer c)
{
    out << setw(35) << left << c.name << setw(30) << left << c.type << setw(7) << left << c.built << setw(7)<< left << c.ybuilt << endl;

    return out;
}
string Computer::getName()
{
    return name;
}
string Computer::getYB()
{
    return ybuilt;
}
string Computer::getType()
{
    return type;
}
string Computer::getBuilt()
{
    return built;
}
