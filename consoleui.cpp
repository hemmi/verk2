#include "consoleui.h"
consoleUI::consoleUI()
{

}

void consoleUI::start()
{
    string action;

    do  //let the user choose which action they would like to perform.
    {
        cout << "Choose which action you would like to perform. The options are:\n" << endl;
        cout << "\tadd: add a new entry to the database.\n";
        cout << "\tview: view a list of database entries.\n";
        cout << "\tconnections: view how scienetists and computers are connected.\n";
        cout << "\tsearch: search through the database.\n";
        cout <<  "\texit: exit the program." << endl;
        cin >> action;

            if(action=="add")
            {
                add();
            }else if(action=="view")
            {
                view();
            }else if(action=="connections")
            {
                connections();
            }else if(action=="search")
            {
                search();
            }else if(action=="exit")
            {
                return;
            }
            else
            {
                cout << "Invalid action please try again!";
            }
    }while(action!="exit");
}
void consoleUI::add()   //add new entry to database.
{
    bool valid = false;
    string action;

    do
    {
        cout << "Would you like to add a computer or a scientist?" << endl;
        cout << "\tcom: add a computer.\n";
        cout << "\tsci: add a scientist.\n";
        cout << "\tback: go back.\n";
        cin >> action;

        if(action=="com")
        {
             addCom();
             valid = true;
        }else if(action=="sci")
        {
            addSci();
            valid = true;
        }else if(action=="back")
        {
           return;
        }else
        {
            cout << "Invalid action please try again!" << endl;
        }
    }
    while(valid==false);
}
void consoleUI::view()
{
    string action;
    bool valid;

    do
    {
        cout << "Would you like to view a list of computers or scientists?" << endl;
        cout << "\tcom: view computers.\n";
        cout << "\tsci: view scientists.\n";
        cout << "\tback: go back.\n";
        cin >> action;

        if(action=="com")
        {
         Corder();
         valid = true;
        }
        else if(action=="sci")
        {
            Sorder();
            valid = true;
        }
        else if(action=="back")
        {
            return;
        }
        else
        {
            cout << "Invalid action please try again!" << endl;
        }
    }
    while(valid==false);
}
void consoleUI::search()
{
    string action;
    bool valid;

    do
    {
        cout << "Would you like to search through computers or scientists?" << endl;
        cout << "\tcom: search through computers.\n";
        cout << "\tsci: search through scientists.\n";
        cout << "\tback: go back.\n";
        cin >> action;

        if(action=="com")
        {
         searchCom();
         valid = true;
        }
        else if(action=="sci")
        {
            searchSci();
            valid = true;
        }
        else if(action=="back")
        {
            return;
        }
        else
        {
            cout << "Invalid action please try again!" << endl;
        }
    }
    while(valid==false);
}
void consoleUI::connections() //show how different computers and scientists are connected.
{
    string action;
    bool valid;

    do
    {
        cout << "Would you like to select a scientist or a computer?" << endl;
        cout << "\tcom: select a computer.\n";
        cout << "\tsci: select a scientist.\n";
        cout << "\tback: go back.\n";
        cin >> action;

        if(action=="com")
        {
             connectedCom();
             valid = true;
        }
        else if(action=="sci")
        {
            connectedSci();
            valid = true;
        }
        else if(action=="back")
        {
           return;
        }
        else
        {
            cout << "Invalid action please try again!" << endl;
        }
    }
    while(valid==false);
}
void consoleUI::addCom()
{
    string name="";
    string ybuilt="";
    string type="";
    string built="";
    string connect;
    bool valid = false;

    do
    {
        cout << "Enter the name of the computer:" << endl;
        cin.ignore(100,'\n');
        getline(cin,name);
        valid = true; //serv.validate(n);
            if(!valid)
            {cout << "Invalid input! Please try again" << endl;}
    }
    while(!valid);

    do
    {
        cout << "Enter the type of the computer:" << endl;
        getline(cin,type);
        valid = true; // serv.validate(yb);
            if(!valid)
            {cout << "Invalid input! Please try again" << endl;}
    }
    while(!valid);

    do
    {
        cout << "Was it ever built?: ('yes' or 'no')" << endl;
        cin >> built;
        valid = val.validateYesOrNo(built);
        val.validateString(built); // Convert first letter to uppercase.
            if(!valid)
            {cout << "Invalid input! Please try again" << endl;}
    }
    while(!valid);

    if(built=="yes")
    {
        do
        {
            cout << "Enter the year it was built::" << endl;
            cin >> ybuilt;
            int yBUILT = atoi(ybuilt.c_str());
            valid = val.validateBirth(yBUILT);
                if(!valid)
                {cout << "Invalid input! Please try again" << endl;}
        }
        while(!valid);
    }

    serv.addCom(name, type, built, ybuilt);

    do
    {
        cout << "Would you like to connect the computer to a scientist \nalready in the database? ('yes' or 'no')" << endl;
        cin >> connect;
        valid = val.validateYesOrNo(connect);
    }
    while(!valid);

    if(connect=="yes")
    {
        connectCom();
    }
}
void consoleUI::searchCom() //search through the computers.
{
    string paramstr;

    cout << "Please enter the term you would like to searh for!" << endl;
    cin.ignore(1,'\n');
    getline(cin, paramstr);
    std::list<Computer> result = serv.search(paramstr);

    listsr(result);
}
void consoleUI::listsr(std::list<Computer> sr)  //display the search results.
{
    cout << setw(35)<< left << "Name" << setw(30)<< left << "Type" << setw(7)<< left << "Built?" << setw(7)<< left << "Year" << endl;
    cout << string(80, '-') <<  endl;

    for(std::list<Computer>::iterator iter = sr.begin(); iter != sr.end(); iter++)
    {
        cout << *iter;
    }

    cout << endl;
    cout << string(80, '-') <<  endl;
}
void consoleUI::Corder()
{
    string order, orderstring;
    bool valid = false;

    do
    {
        cout << "Please select the field by which you would like to sort: \nname, year, type, built" << endl;
        cin >> order;
        valid = val.validsort(order);
            if(!valid)
            {cout << "Invalid input! Please try again" << endl;}
    }
    while(!valid);

    orderstring = serv.assessOrder(order);

    std::list<Computer> sortedlist = serv.sortC(orderstring);

    cout << setw(35)<< left << "Name" << setw(30)<< left << "Type" << setw(7)<< left << "Built?" << setw(7)<< left << "Year" << endl;
    cout << string(80, '-') <<  endl;
    for(std::list<Computer>::iterator iter = sortedlist.begin(); iter != sortedlist.end(); iter++)
    {
        cout << *iter;
    }

    cout << endl << string(80, '-') <<  endl;
}
void consoleUI::connectCom()
{
    int rowID = serv.getRowIDCom(); //get the ID of the computer being added;
    int id;
    bool valid=false;

    vector<int> idlist;
    string prompt;
    std::list<sciID> sID = serv.getSciIDlist();

    cout << setw(5)<< left << "ID" << setw(37)<< left << "Name" << endl;
    cout << string(80, '-') <<  endl;

    for(std::list<sciID>::iterator iter = sID.begin(); iter != sID.end(); iter++)
    {
        cout << *iter;
    }

    cout << endl << string(80, '-') <<  endl;

    do
    {
        cout << "Enter the ID of the scientists(listed above)\n";
        cout << "that you would like to associate with the computer" << endl;
        do{
            cin >> id;
            cin.clear(); //prevent the program from crashing when user inputs a character.
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            valid = val.validateID(sID,id);
            if(!valid){cout << "Invalid input! Please try again" << endl;}
        }while(!valid);
        idlist.push_back(id);

        cout << "Would you like to associate the computer\nwith another scientist?('yes' or 'no')"<< endl;

        do
        {
            cin >> prompt;
            valid = val.validateYesOrNo(prompt);
                if(!valid)
                {cout << "Invalid input! Please try again" << endl;}
        }
        while(!valid);
    }
    while(prompt!="no");

    serv.connectCom(rowID, idlist);
}
void consoleUI::connectedCom()
{
    int id;
    bool valid;
    std::list<sciID> sID = serv.getComIDlist();

    cout << setw(5)<< left << "ID" << setw(37)<< left << "Name" << endl;
    cout << string(80, '-') <<  endl;

    for(std::list<sciID>::iterator iter = sID.begin(); iter != sID.end(); iter++)
    {
        cout << *iter;
    }

    cout << endl << string(80, '-') <<  endl;
    cout << "Enter the ID of the computer(listed above)\n";
    cout << "to see which scientists they are connected to." << endl;
    do{
        cin >> id;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        valid = val.validateID(sID, id);
        if(!valid){cout << "Invalid input! Please try again" << endl;}
    }while(!valid);

    Computer conCom = serv.getConnectedComputer(id);

    cout << "You have selected the following computer:" << endl <<  endl;
    cout << setw(35)<< left << "Name" << setw(30)<< left << "Type" << setw(7)<< left << "Built?" << setw(7)<< left << "Year" << endl;
    cout << string(80, '-') <<  endl;
    cout << conCom << endl;
    cout << string(80, '-') <<  endl;
    cout << "It is connected to the following scientist/s:" << endl << endl;
    cout << setw(30)<< left << "Name" << setw(20)<< left << "Year of Birth" << setw(20)<< left << "Year of Death" << setw(5)<< left<< "Sex" << endl;
    cout << string(80, '-') <<  endl;

    std::list<individual> connectedScientists = serv.ConnectedScientists(id);

    for(std::list<individual>::iterator iter = connectedScientists.begin(); iter != connectedScientists.end(); iter++)
    {
        cout << *iter;
    }

    cout << endl << string(80, '-') <<  endl;
}

void consoleUI::addSci()
{
    string n;
    string yb, yd;
    string s;
    string connect;
    bool valid =false;

    do
    {
        cout << "Enter name:" << endl;
        cin.ignore(100,'\n');
        getline(cin,n);
        valid = true;
            if(!valid)
            {cout << "Invalid input! Please try again" << endl;}
    }
    while(!valid);

    do
    {
        cout << "Enter year of birth:" << endl;
        cin >> yb;
        int numYB = atoi(yb.c_str());
        valid = val.validateBirth(numYB);
            if(!valid)
            {cout << "Invalid input! Please try again" << endl;}
    }
    while(!valid);

    do
    {
        cout << "Enter year of death:(Leave empty if still alive)" << endl;
        cin.ignore(1,'\n');
        getline(cin,yd);
        if(yd.empty())
        {
            valid = true;
        }
        else
        {
            int numYD = atoi(yd.c_str());
            int numYB = atoi(yb.c_str());
            valid =  val.validateDeath(numYD, numYB);
                if(!valid)
                {cout << "Invalid input! Please try again" << endl;}
        }
    }
    while(!valid);

    do
    {
        cout << "Enter sex (The options are \'m\' for male or \'f\' for female):" << endl;
        cin >> s;
        valid = val.validateSex(s);
            if(!valid)
            {cout << "Invalid input! Please try again" << endl;}
    }
    while(!valid);

    serv.addSci(n ,yb ,yd ,s);

    do
    {
        cout << "Would you like to connect the scientist to a computer \nalready in the database? ('yes' or 'no')" << endl;
        cin >> connect;
        valid = val.validateYesOrNo(connect);
    }
    while(!valid);

    if(connect=="yes")
    {
        connectSci();
    }
}
void consoleUI::searchSci() //search through the scientists.
{
    string paramstr;

    cout << "Please enter the term you would like to searh for!" << endl;
    cin.ignore(1,'\n');
    getline(cin, paramstr);
    std::list<individual> result = serv.searchSci(paramstr);

    listsr(result);
}
void consoleUI::listsr(std::list<individual> sr)    //display the search results.
{
    cout << setw(30)<< left << "Name" << setw(20)<< left << "Year of Birth" << setw(20)<< left << "Year of Death" << setw(5)<< left<< "Sex" << endl;
    cout << string(80, '-') <<  endl;

    for(std::list<individual>::iterator iter = sr.begin(); iter != sr.end(); iter++)
    {
        cout << *iter;
    }

    cout << endl;
    cout << string(80, '-') <<  endl;
}
void consoleUI::Sorder()
{
    string order, orderstring;
    bool valid = false;

    do
    {
        cout << "Please select the field by which you would like to sort: \nname, birth, death, sex" << endl;
        cin >> order;
        valid = val.validsortS(order);
            if(!valid)
            {cout << "Invalid input! Please try again" << endl;}
    }
    while(!valid);

    orderstring = serv.assessOrderS(order);

    std::list<individual> sortedlist = serv.sortS(orderstring);
    cout << setw(30)<< left << "Name" << setw(20)<< left << "Year of Birth" << setw(20)<< left << "Year of Death" << setw(5)<< left<< "Sex" << endl;
    cout << string(80, '-') <<  endl;
    for(std::list<individual>::iterator iter = sortedlist.begin(); iter != sortedlist.end(); iter++)
    {
        cout << *iter;
    }

    cout << endl << string(80, '-') <<  endl;
}

void consoleUI::connectSci()
{
    int rowID = serv.getRowIDSci(); //get the ID of the scientist being added;
    int id;
    bool valid=false;
    vector<int> idlist;
    string prompt;
    std::list<sciID> sID = serv.getComIDlist();

    cout << setw(5)<< left << "ID" << setw(37)<< left << "Name" << endl;
    cout << string(80, '-') <<  endl;

    for(std::list<sciID>::iterator iter = sID.begin(); iter != sID.end(); iter++)
    {
        cout << *iter;
    }

    cout << endl << string(80, '-') <<  endl;

    do
    {
        cout << "Enter the ID of the computer(listed above)\n";
        cout << "that you would like to associate with the scientist" << endl;

        do{
                cin >> id;
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                valid = val.validateID(sID,id);
                if(!valid){cout << "Invalid input! Please try again" << endl;}
            }while(!valid);

        idlist.push_back(id);
        cout << "Would you like to associate the scientist\nwith another computer?('yes' or 'no')"<< endl;

        do
        {
            cin >> prompt;
            valid = val.validateYesOrNo(prompt);
                if(!valid)
                {cout << "Invalid input! Please try again" << endl;}
        }
        while(!valid);
    }
    while(prompt!="no");

    serv.connectSci(rowID, idlist);
}

void consoleUI::connectedSci()
{
    int id;
    bool valid;
    std::list<sciID> sID = serv.getSciIDlist();

    cout << setw(5)<< left << "ID" << setw(37)<< left << "Name" << endl;
    cout << string(80, '-') <<  endl;

    for(std::list<sciID>::iterator iter = sID.begin(); iter != sID.end(); iter++)
    {
        cout << *iter;
    }

    cout << endl << string(80, '-') <<  endl;
    cout << "Enter the ID of the scientist(listed above)\n";
    cout << "to see which computers they are connected to." << endl;
    do{
        cin >> id;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        valid = val.validateID(sID, id);
        if(!valid){cout << "Invalid input! Please try again" << endl;}
    }while(!valid);
    individual conSci = serv.getConnectedScientist(id);
    std::list<Computer> connectedComputers = serv.ConnectedComputers(id);

    cout << "You have selected the following scientist:" << endl << endl;
    cout << setw(30)<< left << "Name" << setw(20)<< left << "Year of Birth" << setw(20)<< left << "Year of Death" << setw(5)<< left<< "Sex" << endl;
    cout << string(80, '-') <<  endl;
    cout << conSci << endl;
    cout << string(80, '-') <<  endl;

    cout << "They are connected to the following computer/s:"<< endl << endl;
    cout << setw(35)<< left << "Name" << setw(30)<< left << "Type" << setw(7)<< left << "Built?" << setw(7)<< left << "Year" << endl;
    cout << string(80, '-') <<  endl;
    for(std::list<Computer>::iterator iter = connectedComputers.begin(); iter != connectedComputers.end(); iter++)
    {
        cout << *iter;
    }

    cout<< endl << string(80, '-') <<  endl;
}
