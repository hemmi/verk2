#include "individual.h"

individual::individual()
{
   name = "";
   yearOfBirth = "";
   yearOfDeath = "";
   sex ="";
}

individual::individual(const string& n, const string& yb, const string& yd, const string& s)//constructor with four parameters one for each field;
{
    name = n;
    yearOfBirth = yb;
    yearOfDeath = yd;
    sex = s;
}
ostream& operator<<(ostream& out, individual ind)   //overloading the << operator.
{
    out << setw(30) << left << ind.name << setw(20) << left << ind.yearOfBirth << setw(20) << left << ind.yearOfDeath << setw(5)<< left << ind.sex << endl;
    return out;
}
string individual::getName()
{
    return name;
}
string individual::getYB()
{
    return yearOfBirth;
}
string individual::getYD()
{
    return yearOfDeath;
}
string individual::getSex()
{
    return sex;
}
