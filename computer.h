#ifndef COMPUTER_H
#define COMPUTER_H

#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

class Computer
{
public:
    Computer();
    Computer(const string &n, const string &yb, const string &t, const string &b);
    friend ostream& operator<<(ostream& out, Computer c);

    string getName();
    string getYB();
    string getType();
    string getBuilt();
private:
    string name;
    string ybuilt;
    string type;
    string built;
};

#endif // COMPUTER_H
