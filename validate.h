#ifndef VALIDATE_H
#define VALIDATE_H
#include <string>
#include <vector>
#include <list>
#include "computer.h"
#include "individual.h"
#include "sciID.h"
#include <sstream>


using namespace std;

class validate //validates input from the user.
{
public:

    validate();

    void validateString(string& name);
    bool validsort(string order);
    bool validsortS(string order);
    bool validateDeath(int yearOfDeath, int yearOfBirth);
    bool validateBirth(int year);
    bool validateSex(string &sex);
    bool validateYesOrNo(string input);

    bool validateID(std::list<sciID> list, int id);
};

#endif // VALIDATE_H
