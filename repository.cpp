#include "repository.h"

repository::repository() //add the database.
{
    db = QSqlDatabase::database();
    db = QSqlDatabase::addDatabase("QSQLITE");
    QString dbName = "computer_science.sqlite";
    db.setDatabaseName(dbName);
    db.open();
}

repository::~repository(){
    db.close();
}

void repository::addCom(Computer com)
{
    string name = com.getName();
    string type = com.getType();
    string built = com.getBuilt();
    string yearb = com.getYB();

    QSqlQuery query; //add the new entry to the database.

    query.prepare("INSERT INTO Computers (Name, CType, Built, YBuilt)"
                  "VALUES(:name, :type, :built, :ybuilt)");

    query.bindValue(":name", QString::fromStdString(name));
    query.bindValue(":type", QString::fromStdString(type));
    query.bindValue(":built", QString::fromStdString(built));
    query.bindValue(":ybuilt", QString::fromStdString(yearb));

    query.exec();

}

std::list<Computer> repository::getClist()
{
   return clist;
}
std::list<Computer> repository::sortC(string choice)
{
    clist.clear();

    Computer c;

    string cname;
    string ybuilt;
    string type;
    string built;

    QSqlQuery query;

    query.prepare(QString("SELECT Name, CType, Built, YBuilt FROM Computers ORDER BY %1").arg(QString::fromStdString(choice))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite

    query.exec();

    while(query.next())
    {
        cname = query.value("Name").toString().toStdString();
        ybuilt = query.value("YBuilt").toString().toStdString();
        type = query.value("CType").toString().toStdString();
        built = query.value("Built").toString().toStdString();

        // Load the sorted records into memoroy

        c = Computer(cname, ybuilt, type, built);
        clist.push_back(c);
    }

     return clist;
}
std::list<Computer> repository::find(string searchstr)
{
    clist.clear();

    Computer c;

    string cname;
    string ybuilt;
    string type;
    string built;

    QSqlQuery query;

    query.prepare(QString("SELECT Name, CType, Built, YBuilt FROM Computers WHERE Name || YBuilt ||CType || Built LIKE '%%1%' ORDER BY Name").arg(QString::fromStdString(searchstr))); //Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite

    query.exec();

    while(query.next())
    {
       cname = query.value("Name").toString().toStdString();
       ybuilt = query.value("YBuilt").toString().toStdString();
       type = query.value("CType").toString().toStdString();
       built = query.value("Built").toString().toStdString();

       // Load all records matching the search string into memoroy

       c = Computer(cname, ybuilt, type, built);
       clist.push_back(c);
    }
   return clist;
}
int repository::getRowIDCom()
{

    QSqlQuery query;
    int rowid;

    query.prepare("SELECT MAX(ID) FROM Computers");
    query.exec();

    while(query.next())
    {
        rowid = query.value("MAX(ID)").toInt();
    }

    return rowid;
}
Computer repository::getConnectedComputer(int id)
{
    Computer c;

    string cname;
    string ybuilt;
    string type;
    string built;

    QSqlQuery query;

    query.prepare(QString("SELECT * FROM Computers WHERE ID is %1").arg(QString::number(id))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();

    while(query.next())
    {
        cname = query.value("Name").toString().toStdString();
        ybuilt = query.value("YBuilt").toString().toStdString();
        type = query.value("CType").toString().toStdString();
        built = query.value("Built").toString().toStdString();

        c = Computer(cname, ybuilt, type, built);
    }
    return c;
}
std::list<sciID> repository::computersID()
{
    IDlist.clear();

    sciID i;
    string id, name;

    QSqlQuery query;

    query.prepare(QString("SELECT ID, Name FROM Computers ORDER BY ID"));

    query.exec();

    while(query.next())
    {
        id = query.value("ID").toString().toStdString();
        name = query.value("Name").toString().toStdString();

        i = sciID(id,name);
        IDlist.push_back(i);
    }

    return IDlist;
}
void repository::connectCom(int id, vector<int> idlist)
{


    for(unsigned int i = 0; i < idlist.size(); i++)
    {
        QSqlQuery query; //add the new entry to the database.

        query.prepare("INSERT INTO Connection (C_Id, S_Id)"
                      "VALUES(:cid, :sid)");
        query.bindValue(":cid", QString::number(id));
        query.bindValue(":sid", QString::number(idlist[i]));
        query.exec();
    }

}
std::list<Computer> repository::ConnectedComputers(int id)
{
    Computer c;
    vector<int> tempList;
    clist.clear();

    int c_id;



    QSqlQuery query;

    query.prepare(QString("SELECT C_Id FROM Connection WHERE S_ID is %1").arg(QString::number(id))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();

    while(query.next())
    {
        c_id = query.value("C_Id").toInt();
        tempList.push_back(c_id);
    }

    for(unsigned int i = 0; i < tempList.size(); i++)
    {
        c =  getConnectedComputer(tempList[i]);

        clist.push_back(c);
    }
    return clist;
}

void repository::addSci(individual ind)
{
    string name = ind.getName();
    string yearb = ind.getYB();
    string yeard = ind.getYD();
    string sex = ind.getSex();

    QSqlQuery query; //add the new entry to the database.

    query.prepare("INSERT INTO Scientists (Name, YoB, YoD, Sex)"
                  "VALUES(:name, :yearb, :yeard, :sex)");

    query.bindValue(":name", QString::fromStdString(name));
    query.bindValue(":yearb", QString::fromStdString(yearb));
    query.bindValue(":yeard", QString::fromStdString(yeard));
    query.bindValue(":sex", QString::fromStdString(sex));

    query.exec();

}
std::list<individual> repository::getSlist()
{
   return slist;
}
std::list<individual> repository::sortS(string choice)
{
    slist.clear();
    individual ind;
    string name;
    string yearB, yearD;
    string sex;

    //Retrieve scientists

    QSqlQuery query;

    query.prepare(QString("SELECT Name, YoB, YoD, Sex FROM Scientists ORDER BY %1").arg(QString::fromStdString(choice))); //Using agruments in QString Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite

    query.exec();

    while(query.next())
    {
        name = query.value("Name").toString().toStdString();
        yearB = query.value("YoB").toString().toStdString();
        yearD = query.value("YoD").toString().toStdString();
        sex = query.value("Sex").toString().toStdString();

        // Load all records into memoroy

        ind = individual(name, yearB, yearD, sex);
        slist.push_back(ind);
    }
    return slist;
}

std::list<individual> repository::findSci(string searchstr)
{
    slist.clear();

    individual i;
    string name;
    string yearB, yearD;
    string sex;

    QSqlQuery query;

    query.prepare(QString("SELECT Name, YoB, YoD, Sex FROM Scientists WHERE Name || YoB ||YoD || Sex LIKE '%%1%' ORDER BY Name").arg(QString::fromStdString(searchstr))); //Using aguments in QString Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite

    query.exec();

    while(query.next())
    {
       name = query.value("Name").toString().toStdString();
       yearB = query.value("YoB").toString().toStdString();
       yearD = query.value("YoD").toString().toStdString();
       sex = query.value("Sex").toString().toStdString();

       // Load all records matching the search string into memoroy

       i = individual(name, yearB, yearD, sex);
       slist.push_back(i);
    }
    return slist;
}

int repository::getRowIDSci()
{

    QSqlQuery query;
    int rowid;

    query.prepare("SELECT MAX(ID) FROM Scientists");
    query.exec();

    while(query.next())
    {
        rowid = query.value("MAX(ID)").toInt();
    }

    return rowid;
}
individual repository::getConnectedScientist(int id)
{
    individual i;

    string name;
    string yearB, yearD;
    string sex;

    QSqlQuery query;

    query.prepare(QString("SELECT Name, YoB, YoD, Sex FROM Scientists WHERE ID is %1").arg(QString::number(id))); //Using aguments in QString Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();

    while(query.next())
    {
       name = query.value("Name").toString().toStdString();
       yearB = query.value("YoB").toString().toStdString();
       yearD = query.value("YoD").toString().toStdString();
       sex = query.value("Sex").toString().toStdString();

       i = individual(name, yearB, yearD, sex);
    }
    return i;
}

std::list<sciID> repository::scientistsID()
{
    IDlist.clear();

    sciID i;

    string id,name;

    QSqlQuery query;

    query.prepare(QString("SELECT ID, Name FROM Scientists ORDER BY ID"));

    query.exec();

    while(query.next())
    {
        id = query.value("ID").toString().toStdString();
        name = query.value("Name").toString().toStdString();

        i = sciID(id,name);
        IDlist.push_back(i);
    }

    return IDlist;
}

void repository::connectSci(int id, vector<int> idlist)
{

    for(unsigned int i=0;i<idlist.size();i++)
    {
        QSqlQuery query; //add the new entry to the database.

        query.prepare("INSERT INTO Connection (C_Id, S_Id)"
                      "VALUES(:cid, :sid)");
        query.bindValue(":cid", QString::number(idlist[i]));
        query.bindValue(":sid", QString::number(id));
        query.exec();
    }

}

std::list<individual> repository::ConnectedScientists(int id)
{
    individual ind;
    vector<int> tempList;
    slist.clear();

    int s_id;

    QSqlQuery query;

    query.prepare(QString("SELECT S_Id FROM Connection WHERE c_Id is %1").arg(QString::number(id))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();

    while(query.next())
    {
        s_id = query.value("S_Id").toInt();
        tempList.push_back(s_id);
    }

    for(unsigned int i = 0; i < tempList.size(); i++)
    {
        ind =  getConnectedScientist(tempList[i]);
       slist.push_back(ind);
    }
    return slist;
}
