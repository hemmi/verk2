#include "sciID.h"

sciID::sciID()
{

}
sciID::sciID(const string& i, const string& n)
{
    id = i;
    name = n;
}

string sciID::getID()
{
    return id;
}
ostream& operator<<(ostream& out, sciID s)  //overloading the << operator.
{
    out << setw(5) << left << s.id << setw(37) << left << s.name << endl;
    return out;
}
